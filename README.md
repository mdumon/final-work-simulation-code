# Predictive Accuracy - Recommendation Engine

Welcome to the Predictive Accuracyrepository! This repository contains the code for our open source Python recommendation engine, designed specifically for this project. Below, you will find an overview of the main features of our library:

## Features

- **Easy dataset handling**: Our library allows for effortless handling of datasets. In addition to the built-in Movielens datasets, you can also use custom datasets.
- **Ready-to-use algorithms**: The algorithms mentioned in this work are readily available and do not require any prior adjustment.
- **Flexible parameter customization**: The parameters of the proposed algorithms can be easily modified to suit your specific needs.

## Download

The library can be downloaded in zip format from the **Bitbucket Downloads Folder**, or you can install it via pip using the following command:

- **pip install scikit-predictiveaccuracy==1.0.0**

Feel free to explore our code and adapt it to your own projects. We hope that Predictive Accuracy will be a valuable resource for your recommendation system research.